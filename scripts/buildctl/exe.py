import json
import os
import shutil
import subprocess

from constants import EXE_ASSETS_DIR, PYINSTALLER_DIR
from utils import copy_directory_contents_into


DISTRIBUTION_SOURCE = 'exe'


def make_exe(workdir, venv, cleanup=True):
    delete_existing_exe_build(workdir)
    do_make_exe(workdir, venv)
    if cleanup:
        cleanup_build(workdir)


def do_make_exe(workdir, venv):
    exe_dir = os.path.join(workdir, 'aws')
    output_exe_dist_dir = os.path.join(exe_dir, 'dist')
    aws_exe_build = pyinstaller('aws.spec', workdir, venv)
    copy_directory(aws_exe_build, output_exe_dist_dir)
    aws_complete_exe_build = pyinstaller('aws_completer.spec', workdir, venv)
    update_metadata(aws_complete_exe_build,
                    distribution_source=DISTRIBUTION_SOURCE)
    copy_directory_contents_into(aws_complete_exe_build, output_exe_dist_dir)
    copy_directory_contents_into(EXE_ASSETS_DIR, exe_dir)


def delete_existing_exe_build(workdir):
    build_dir = os.path.join(workdir, 'dist')
    if os.path.isdir(build_dir):
        shutil.rmtree(build_dir)


def pyinstaller(specfile, workspace, venv):
    aws_spec_path = os.path.join(PYINSTALLER_DIR, specfile)
    subprocess.run(
        [
            venv.python_exe,
            os.path.join(venv.bin_dir, 'pyinstaller'),
            aws_spec_path,
            '--distpath', os.path.join(workspace, 'dist'),
            '--workpath', os.path.join(workspace, 'build'),
        ], cwd=PYINSTALLER_DIR, check=True
    )
    return os.path.join(
        workspace, 'dist', os.path.splitext(specfile)[0])


def copy_directory(src, dst):
    print('Copying %s -> %s' % (src, dst))
    shutil.copytree(src, dst)


def cleanup_build(workdir):
    locations = [
        os.path.join(workdir, 'build'),
        os.path.join(workdir, 'dist'),
    ]
    for location in locations:
        shutil.rmtree(location)
        print('Deleted build directory: %s' % location)


def update_metadata(dirname, **kwargs):
    print('Update metadata values %s' % kwargs)
    metadata_file = os.path.join(dirname, 'awscli', 'data', 'metadata.json')
    with open(metadata_file) as f:
        metadata = json.load(f)
    for key, value in kwargs.items():
        metadata[key] = value
    with open(metadata_file, 'w') as f:
        json.dump(metadata, f)
