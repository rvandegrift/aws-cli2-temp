import importlib.metadata
# TODO: pkg_resources is discouraged in favor of importlib but it does not
#   have the requirement parsing we will probably need to roll out our own
#   parser as we cannot guarantee pkg_resoure's existence with setuptools in
#   the future.
from pkg_resources import parse_requirements

from constants import BOOTSTRAP_REQUIREMENTS, BUILD_REQUIREMENTS
from utils import get_install_requires


class UnmetDependenciesException(Exception):
    def __init__(self, unmet_deps):
        msg = 'Environment requires following Python dependencies:\n\n'
        for package, actual_version, required in unmet_deps:
            msg += (
                f'{package} (required: {required}) '
                f'(version installed: {actual_version})\n'
            )
        super().__init__(msg)


def validate_env(target_artifact):
    requirements = _get_requires_list(target_artifact)
    unmet_deps = _get_unmet_dependencies(requirements)
    if unmet_deps:
        raise UnmetDependenciesException(unmet_deps)


def _get_requires_list(target_artifact):
    requires_list = _parse_requirements(BOOTSTRAP_REQUIREMENTS)
    requires_list += get_install_requires()
    if target_artifact == 'portable-exe':
        requires_list += _parse_requirements(BUILD_REQUIREMENTS)
    return list(parse_requirements(requires_list))


def _parse_requirements(requirements_file):
    requirements = []
    with open(requirements_file, 'r') as f:
        for line in f.readlines():
            if not line.startswith(('-r', '#')):
                requirements.append(line.strip())
    return requirements


def _get_unmet_dependencies(requirements):
    unmet = []
    for requirement in requirements:
        project_name = requirement.project_name
        try:
            actual_version = importlib.metadata.version(project_name)
        except importlib.metadata.PackageNotFoundError:
            unmet.append((project_name, None, requirement))
            continue
        if actual_version not in requirement:
            unmet.append((project_name, actual_version, requirement))
    return unmet

