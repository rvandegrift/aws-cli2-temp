import argparse
import os
import shutil

from awscli_venv import AwsCliVenv
from constants import (
    BUILD_DIR, BOOTSTRAP_REQUIREMENTS_LOCK, RUNTIME_REQUIREMENTS_LOCK,
    BUILD_REQUIREMENTS_LOCK, INSTALL_DIRNAME
)
from exe import make_exe
from install import do_install, do_uninstall
from validate_env import validate_env


def create_awscli_venv(build_dir, download_deps):
    _bootstap_venv(build_dir, download_deps, RUNTIME_REQUIREMENTS_LOCK)


def create_exe(build_dir, download_deps):
    aws_venv = _bootstap_venv(
        build_dir, download_deps, BUILD_REQUIREMENTS_LOCK)
    exe_workspace = os.path.join(build_dir, 'exe')
    if os.path.exists(exe_workspace):
        shutil.rmtree(exe_workspace)
    make_exe(exe_workspace, aws_venv)


def build(parsed_args):
    if parsed_args.artifact == 'portable-exe':
        create_exe(parsed_args.build_dir, parsed_args.download_deps)
    else:
        create_awscli_venv(parsed_args.build_dir, parsed_args.download_deps)


def validate(parsed_args):
    validate_env(parsed_args.artifact)


def install(parsed_args):
    build_dir = parsed_args.build_dir
    install_dir = os.path.join(parsed_args.lib_dir, INSTALL_DIRNAME)
    bin_dir = parsed_args.bin_dir
    do_install(build_dir, install_dir, bin_dir)


def uninstall(parsed_args):
    install_dir = os.path.join(parsed_args.lib_dir, INSTALL_DIRNAME)
    bin_dir = parsed_args.bin_dir
    do_uninstall(install_dir, bin_dir)


def _bootstap_venv(build_dir, download_deps, requirements):
    venv_path = os.path.join(build_dir, 'venv')
    if os.path.exists(venv_path):
        shutil.rmtree(venv_path)
    os.makedirs(venv_path)
    aws_venv = AwsCliVenv(venv_path)
    aws_venv.create()
    if download_deps:
        aws_venv.install_requirements(BOOTSTRAP_REQUIREMENTS_LOCK)
        aws_venv.install_requirements(requirements)
    else:
        aws_venv.copy_parent_packages()
    aws_venv.install_awscli()
    return aws_venv


def main():
    parser = argparse.ArgumentParser()

    subparser = parser.add_subparsers()

    validate_env_parser = subparser.add_parser('validate-env')
    validate_env_parser.add_argument(
        '--artifact', choices=['portable-exe', 'system-sandbox'],
        required=True
    )
    validate_env_parser.set_defaults(func=validate)

    build_parser = subparser.add_parser('build')
    build_parser.add_argument(
        '--artifact', choices=['portable-exe', 'system-sandbox'],
        required=True
    )
    build_parser.add_argument(
        '--build-dir', default=BUILD_DIR, type=os.path.abspath
    )
    build_parser.add_argument(
        '--download-deps', action='store_true'
    )
    build_parser.set_defaults(func=build)

    install_parser = subparser.add_parser('install')
    install_parser.add_argument(
        '--build-dir', default=BUILD_DIR, type=os.path.abspath
    )
    install_parser.add_argument('--lib-dir', required=True,
                                type=os.path.abspath)
    install_parser.add_argument('--bin-dir', required=True,
                                type=os.path.abspath)
    install_parser.set_defaults(func=install)

    uninstall_parser = subparser.add_parser('uninstall')
    uninstall_parser.add_argument('--lib-dir', required=True,
                                  type=os.path.abspath)
    uninstall_parser.add_argument('--bin-dir', required=True,
                                  type=os.path.abspath)
    uninstall_parser.set_defaults(func=uninstall)

    parsed_args = parser.parse_args()
    parsed_args.func(parsed_args)


if __name__ == '__main__':
     main()
