import os
import sys

from constants import ROOT_DIR
sys.path.append(ROOT_DIR)

from awscli.autocomplete.local import indexer
from awscli.autocomplete.serverside.indexer import APICallIndexer
from awscli.autocomplete import db
from awscli.autocomplete import generator
from awscli import clidriver


def generate_index(filename):
    filename = os.path.abspath(filename)
    index_dir = os.path.dirname(filename)
    if not os.path.isdir(index_dir):
        os.makedirs(index_dir)

    # Using a temporary name so if the index already exists, we'll
    # only replace the entire file once we successfully regenerate the
    # index.
    temp_name = f'{filename}.temp'
    db_connection = db.DatabaseConnection(temp_name)
    indexers = [
        indexer.ModelIndexer(db_connection),
        APICallIndexer(db_connection),
    ]
    driver = clidriver.create_clidriver()
    index_gen = generator.IndexGenerator(indexers=indexers)
    try:
        index_gen.generate_index(driver)
    finally:
        db_connection.close()
    os.rename(temp_name, filename)
    return filename
