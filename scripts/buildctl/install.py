import os
import shutil
import sys

from constants import CLI_EXECUTABLES


def do_install(build_dir, install_dir, bin_dir):
    artifact_type = _get_artifact_type(build_dir)
    _copy_to_install_dir(build_dir, install_dir, artifact_type)
    _symlink_executables(install_dir, bin_dir, artifact_type)


def do_uninstall(install_dir, bin_dir):
    if os.path.isdir(install_dir):
        shutil.rmtree(install_dir)
    for exe in CLI_EXECUTABLES:
        exe_path = os.path.join(bin_dir, exe)
        if os.path.islink(exe_path):
            os.remove(exe_path)


def _get_artifact_type(build_dir):
    if os.path.isdir(os.path.join(build_dir, 'exe')):
        return 'portable-exe'
    else:
        return 'system-sandbox'


def _copy_to_install_dir(build_dir, install_dir, artifact_type):
    build_lib = _get_build_lib(build_dir, artifact_type)
    if os.path.isdir(install_dir):
        shutil.rmtree(install_dir)
    shutil.copytree(build_lib, install_dir)
    if artifact_type == 'system-sandbox':
        _update_script_header(install_dir)


def _update_script_header(install_dir):
    python_exe_name = 'python'
    if sys.platform == 'win32':
        python_exe_name = 'python.exe'
    python_exe_path = _get_install_bin_exe(
        install_dir, python_exe_name, 'system-sandbox')
    for exe in CLI_EXECUTABLES:
        exe_path = _get_install_bin_exe(install_dir, exe, 'system-sandbox')
        with open(exe_path) as f:
            lines = f.readlines()
        lines[0] = _get_script_header(python_exe_path)
        with open(exe_path, 'w') as f:
            f.write(''.join(lines))


def _get_script_header(python_exe_path):
    if sys.platform == 'win32':
        return f'@echo off & "{python_exe_path}" -x "%~f0" %* & goto :eof\n'
    return f'#!{python_exe_path}\n'


def _symlink_executables(install_dir, bin_dir, artifact_type):
    if not os.path.exists(bin_dir):
        os.makedirs(bin_dir)
    for exe in CLI_EXECUTABLES:
        exe_path = os.path.join(bin_dir, exe)
        if os.path.islink(exe_path):
            os.remove(exe_path)
        os.symlink(
            _get_install_bin_exe(install_dir, exe, artifact_type),
            exe_path
        )


def _get_build_lib(build_dir, artifact_type):
    if artifact_type == 'portable-exe':
        return os.path.join(build_dir, 'exe', 'aws', 'dist')
    else:
        return os.path.join(build_dir, 'venv')


def _get_install_bin_exe(install_dir, exe, artifact_type):
    install_bin_dir = install_dir
    if artifact_type == 'system-sandbox':
        bin_dirname = 'bin'
        if sys.platform == 'win32':
            bin_dirname = 'Scripts'
        install_bin_dir = os.path.join(install_dir, bin_dirname)
    return os.path.join(install_bin_dir, exe)
