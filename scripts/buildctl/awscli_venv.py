import os
import subprocess
import site
import sys
import venv

from constants import ROOT_DIR
from utils import copy_file, copy_directory_contents_into


class AwsCliVenv:
    _PARENT_SCRIPTS_TO_COPY = [
        'pyinstaller',
    ]

    def __init__(self, venv_dir):
        self._venv_dir = venv_dir

    def create(self):
        venv.create(self._venv_dir, with_pip=True)

    def copy_parent_packages(self):
        for site_package in site.getsitepackages():
            copy_directory_contents_into(site_package, self._site_packages())
        parent_scripts = os.path.dirname(
            os.path.realpath(sys.executable)
        )
        for script in self._PARENT_SCRIPTS_TO_COPY:
            source = os.path.join(parent_scripts, script)
            if os.path.exists(source):
                copy_file(source, os.path.join(self.bin_dir, script))

    def install_requirements(self, requirements_file):
        self._pip_install(['-r', requirements_file])

    def install_awscli(self):
        self._pip_install(
            [
                ROOT_DIR, '--no-build-isolation',
                '--use-feature=in-tree-build', '--no-cache-dir', '--no-index'
            ]
        )

    def archive(self):
        pass

    @property
    def bin_dir(self):
        if sys.platform == 'win32':
            return os.path.join(self._venv_dir, 'Scripts')
        else:
            return os.path.join(self._venv_dir, 'bin')

    @property
    def python_exe(self):
        exe_name = 'python'
        if sys.platform == 'win32':
            exe_name += '.exe'
        return os.path.join(self.bin_dir, exe_name)

    def _pip_install(self, args):
        args = [self.python_exe, '-m', 'pip', 'install'] + args
        run_kwargs = {'check': True}
        if sys.platform == 'win32':
            args = ' '.join(args)
            run_kwargs['shell'] = True
        subprocess.run(args, **run_kwargs)

    def _site_packages(self):
        # TODO: We should figure out a more programmatic way of finding this...
        if sys.platform == 'win32':
            return os.path.join(
                self._venv_dir, 'Lib', 'site-packages')
        else:
            python_version = (
                f"python{sys.version_info[0]}.{sys.version_info[1]}")
            return os.path.join(
                self._venv_dir, 'lib', python_version, 'site-packages')
