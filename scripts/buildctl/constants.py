import os
import sys


ROOT_DIR = os.path.dirname(
    os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))
    )
)
BUILD_DIR = os.path.join(ROOT_DIR, 'build')

EXE_DIR = os.path.join(ROOT_DIR, 'exe')
EXE_ASSETS_DIR = os.path.join(EXE_DIR, 'assets')
PYINSTALLER_DIR = os.path.join(EXE_DIR, 'pyinstaller')

# Requirements files
REQUIREMENTS_DIR = os.path.join(ROOT_DIR, 'requirements')
BOOTSTRAP_REQUIREMENTS = os.path.join(
    REQUIREMENTS_DIR, 'bootstrap.txt')
BOOTSTRAP_REQUIREMENTS_LOCK = os.path.join(
    REQUIREMENTS_DIR, 'bootstrap-lock.txt')
RUNTIME_REQUIREMENTS_LOCK = os.path.join(
    REQUIREMENTS_DIR, 'runtime-lock.txt')
BUILD_REQUIREMENTS = os.path.join(
    REQUIREMENTS_DIR, 'build.txt')
BUILD_REQUIREMENTS_LOCK = os.path.join(
    REQUIREMENTS_DIR, 'build-lock.txt')

# Auto-complete index
AC_INDEX = os.path.join(ROOT_DIR, 'awscli', 'data', 'ac.index')

INSTALL_DIRNAME = 'aws-cli'

CLI_EXECUTABLES = ['aws', 'aws_completer']
if sys.platform == 'win32':
    CLI_EXECUTABLES = ['aws.cmd']
