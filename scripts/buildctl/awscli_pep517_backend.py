import os
import setuptools.build_meta

from constants import AC_INDEX
from utils import get_install_requires


def build_sdist(sdist_directory, config_settings=None):
    _build_ac_index_if_needed()
    return setuptools.build_meta.build_sdist(sdist_directory, config_settings)


def build_wheel(wheel_directory, config_settings=None,
                metadata_directory=None):
    _build_ac_index_if_needed()
    return setuptools.build_meta.build_wheel(
            wheel_directory, config_settings, metadata_directory)


def get_requires_for_build_sdist(config_settings=None):
    return _get_extra_build_deps()


def get_requires_for_build_wheel(config_settings=None):
    return _get_extra_build_deps()


def _build_ac_index_if_needed():
    if _needs_built_ac_index():
        from ac_index import generate_index
        generate_index(AC_INDEX)


def _needs_built_ac_index():
    return not os.path.exists(AC_INDEX)


def _get_extra_build_deps():
    if _needs_built_ac_index():
        return get_install_requires()
    return []
