import os
import shutil

from setuptools.config import read_configuration
from distutils.dir_util import copy_tree

from constants import ROOT_DIR


def copy_file(src, dst):
    print('Copying file %s -> %s' % (src, dst))
    shutil.copy2(src, dst)


def copy_directory_contents_into(src, dst):
    print('Copying contents of %s into %s' % (src, dst))
    copy_tree(src, dst)


def get_install_requires():
    return read_configuration(
        os.path.join(ROOT_DIR, 'setup.cfg'))['options']['install_requires']
